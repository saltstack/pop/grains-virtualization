import os
import pytest
import mock


@pytest.mark.asyncio
async def test_load_virtual(mock_hub, hub):
    mock_hub.grains.virtual.virtual = hub.grains.virtual.virtual
    mock_hub.grains.virtual.virtual.load_virtual()
